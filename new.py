import os
import re
import numpy as np
import matplotlib.pyplot as plt
from math import *


def extraire_coordonnees(fichier):
    f = open(fichier, 'r')
    lines = f.readlines()
    coordonnees = []
    for line in lines:
        if not "#" in line and "," in line:
            line = line.replace(",", ".")
            line = line.split()
            try:
                coordonnees.append(
                    ((float(line[1]) / 0.125) * 0.1, (float(line[2]) / 0.125) * 0.1, ((float(line[0])) / 0.125) * 0.1))
            except IndexError:
                pass
    return np.array(coordonnees)


def trouver_centre(points: np.ndarray):
    distances = []
    centres = []
    for point in points:
        distance_max = [(float, float), (float, float), 0]
        for point2 in points:
            d = [(point[0], point[1]), (point2[0], point2[1]),
                 sqrt((point[1] - point2[1]) ** 2 + (point[0] - point2[0]) ** 2)]
            if d[2] > distance_max[2]:
                distance_max = d
        distances.append(distance_max)
    for element in distances:
        centres.append([(element[0][0] + element[1][0]) / 2, (element[0][1] + element[1][1]) / 2])
    return np.mean(centres, axis=0)


def trouver_rayon(centre_cercle: tuple, cercle: np.ndarray):
    rayons = np.array([])
    for point in cercle:
        np_value = sqrt((point[1] - centre_cercle[1]) ** 2 + (point[0] - centre_cercle[0]) ** 2)
        rayons = np.append(rayons, np_value)
    return np.mean(rayons)


def tracer_points_cercle(points: np.ndarray, c: tuple, rayon: float, titre: str):
    x = [p[0] for p in points]
    y = [p[1] for p in points]
    x0, y0 = c
    plt.scatter(x0, y0)
    plt.scatter(x, y)
    plt.scatter(x[0], y[0])
    ax = plt.subplot()
    plt.axis('equal')
    circle = plt.Circle((x0, y0), rayon, fill=False, color='r')
    ax.add_patch(circle)
    plt.title(titre)
    plt.show()


def calcul_vitesse_angulaire(coordonnees: list, centre: tuple, rayon: float, methode=0):
    global echantillonnage
    if methode == 0:  # calculs par les angles
        tous_les_dix = coordonnees[::echantillonnage]
        vitesses_angulaires = np.array([])
        x0, y0 = centre
        for i in range(len(tous_les_dix) - 1):
            try:
                x1, y1, t1 = tous_les_dix[i]
                x2, y2, t2 = tous_les_dix[i + 1]
                #  omega = acos(((x1-x0)*(x2-x0)+(y1-y0)*(y2-y0))/sqrt((x1-x0)**2+(y1-y0)**2)*sqrt((x2-x0)**2+(y2-y0)**2))/(t1-t2)
                xm = (x1 + x2) / 2
                ym = (y1 + y2) / 2
                omega = 2 * acos(sqrt((x0 - xm) ** 2 + (y0 - ym) ** 2) / rayon) / (t2 - t1)
                vitesses_angulaires = np.append(vitesses_angulaires, omega)
            except ValueError:
                pass
        x = [i for i in range(len(vitesses_angulaires))]
        vitesse_angulaire = np.mean(vitesses_angulaires)
    return vitesse_angulaire


def calcul_masses(points, centre_non_normalise, rayon):
    R_0 = 1.95e-1
    K = 9.59e1
    K = 10 / (19.5e-2 - 11.7e-2)
    vitesse_angulaire = calcul_vitesse_angulaire(points, centre_non_normalise, rayon)
    m = (K * ((rayon - 0.055) - 0.14)) / (rayon * vitesse_angulaire ** 2)
    return m


def exploiter_fichier(fichier_utilise: str, trace=0):
    global masses
    global masses_norm
    global masses_attendues
    data = extraire_coordonnees(fichier_utilise)
    centre = trouver_centre(data)
    r = trouver_rayon(centre, data)
    if trace:
        tracer_points_cercle(data, centre, r, titre="Données brutes")
    masse_calculee= calcul_masses(data, centre, r)
    try:
        masse_attendue = [int(num) for num in re.findall(r'\d{1,3}', fichier_utilise)][0]
    except:
        masse_attendue = "?"
    masses.append(masse_calculee)
    masses_attendues.append(masse_attendue*0.001)
    print(f"Fichier \"{fichier}\"")
    print(f"La masse calculée non normalisée est : {masse_calculee} kg.", end="\n\n")


def comparer_resultats(liste_masses: list, liste_masses_attendues: list, offset: bool):
    global plage_valeurs_attendues
    global offset_masses
    diff_att = np.array([])
    for m, m_att in zip(liste_masses, liste_masses_attendues):
        if m_att in plage_valeurs_attendues:
            diff_att = np.append(diff_att, abs(m - m_att))
    assert len(masses_attendues) == len(liste_masses), "Erreur récupération masses attendues"
    plt.figure()
    if offset:
        plt.plot(masses_attendues, [masse + offset_masses for masse in liste_masses], "bo",)
        plt.title("Masse obtenue en fonction de la masse attendue (en kg), avec offset")
    else:
        print(f"Liste des masses attendues (en kg) : {str(liste_masses_attendues).strip('()')}.")
        print(f"Différence moyenne de {round(np.mean(diff_att) * 1000)} grammes entre masses brutes et attendues entre {round(min(plage_valeurs_attendues) * 1000)}g et {round(max(plage_valeurs_attendues) * 1000)}g.", end="\n\n")
        plt.plot(masses_attendues, liste_masses, "bo")
        plt.title("Masse obtenue en fonction de la masse attendue (en kg), sans offset")
        plt.xlabel("Masse attendue (kg)")
        plt.ylabel("Masse calculée (kg)")
    plt.axis([0, None, 0, None])


liste = os.listdir(".")
masses = []
masses_attendues = []
echantillonnage = 5  # Valeur retenue : 5
offset_masses = 0
plage_valeurs_attendues = [0.1, 0.11, 0.12, 0.13]
for fichier in liste:
    if ".mecavideo" in fichier:
        exploiter_fichier(fichier)
z = list(zip(masses_attendues, masses))
zs = sorted(z, key=lambda x: x[0])
masses_attendues, masses = zip(*zs)
comparer_resultats(masses, masses_attendues, offset=False)
#comparer_resultats(masses, masses_attendues, offset=True)
plt.show()

